/* Name   : BinaryImage.java
   Author : SAURAV KC
   Description : 2D Binary image in Java  */
import java.io.*;
public class BinaryImage {

void locatepixels( int ar[][], int i, int j){
  System.out.println( "The array is: \n");
  for( int a = 0 ;a < i; a++){
    for( int b = 0; b < j; b++){
      System.out.println( ar[a][b] + " ");
      if( b == j-1 ) System.out.println("\n");
    }
  }
  System.out.println( "The pixels are located at: \n");
  for( int a = 0 ;a < i; a++){
    for( int b = 0; b < j; b++){
      if( ar[a][b] >= 1 ){ System.out.println("( " + a + ", " + b + " )\n"); }
    }
  }
}

void Access2dImage( String theFile ) throws Exception{
  BufferedReader inFile = new BufferedReader(new FileReader(theFile));
  String lin = inFile.readLine(); // Read first line 12 8 0 1
  int i = lin.indexOf(" ");
  int Rows = Integer.parseInt(lin.substring(0, i));
  lin = lin.substring(i+1); // Eliminate row value and space
  i = lin.indexOf(" ");
  int Cols = Integer.parseInt(lin.substring(0, i));
  int[][]  arr = new int[Rows][Cols];
  for(int p = 0; p<Rows; p++)
    for(int q = 0; q<Cols; q++)
        arr[p][q] = 0;
  
  // Populate arrays with values from next line
  lin = inFile.readLine();
  i = 0;
  while( lin!=null && !(lin.isEmpty()) ) {
    String[] values = lin.trim().split("\\s+");
    for( int j = 0; j < values.length ; j++ ){
      if ( j == values.length ){ i++; }
      arr[i][j] = Integer.parseInt( values[j] );
      System.out.println( arr[i][j] );
    }
    lin = inFile.readLine();
  }
  locatepixels(arr, Rows, Cols);
  inFile.close();
}

public static void main(String[] args) throws Exception {
  if( args.length != 1 ){
    System.out.println("Format: " + " <BinaryImagefilename> ");
    return;
  }
  BinaryImage b = new BinaryImage();
  String bi_file = args[0];
  b.Access2dImage(bi_file);
  }
}