/* Name   : BinaryImage.cpp
   Author : SAURAV KC
   Description : 2D Binary image in C++  */
#include <iostream>
#include <fstream>
#include <cmath>
using namespace std;

void locatepixels( int** ar, int i, int j){
  cout<< "The array is: \n";
  for( int a = 0 ;a < i; a++ ){
    for( int b = 0; b < j; b++ ){
      cout<< ar[a][b] << " ";
      if( b == j-1) cout << endl;
    }
  }
  cout<< "The pixels are located at: \n";
  for( int a = 0 ;a < i; a++){
    for( int b = 0; b < j; b++){
      if( ar[a][b] >= 1 ){ cout << "( " << a << ", " << b << " )\n"; }
    }
  }
}

void Access2dImage( const string& s ){
  ifstream in(s); // open the input file
  string str;
  int i,j; // Binary image row n col

  getline(in,str); // Read first line 12 8 0 1
  i = stoi(str.substr(0, str.find(' ')));
  j = stoi(str.substr(log10(i) + 1, str.find(' '))); // substring counting i's digit

  // Allocate a 2D array
  int** arr = new int*[i]; // pointer for row elements containing pt to sub-arrays (columns)
  for( int k = 0; k < i; k++){  arr[k] = new int[j];  }

  // Populate the array
  for( int a = 0 ;a < i; a++){
    getline(in, str);	//cout<< str << endl;
    for( int b = 0; b < j; b++){
    	//cout<< str.substr( 2*b, str.find(' ') ) << ' ';
    	//if( b == j-1) cout << endl;
    	arr[a][b] = stoi(str.substr( 2*b , str.find(' '))); // 0 or 1 separated by space
    }
  }
  locatepixels(arr,i,j);
  in.close();
}

int main(int argc, char** argv) {
  if(argc != 2){
    cout << "Format: " << argv[0] << " <BinaryImagefilename> " << endl;
    return 0;
  }
  const string bi_file(argv[1]);
  Access2dImage(bi_file);
  return 0;
}
